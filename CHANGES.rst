Changelog
=========

1.0.0 (2023-03-27)
------------------

-   First release. Schema have been migrated from
    https://git.ligo.org/emfollow/userguide/-/tree/main/_static.
